from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import unix_timestamp, to_date
from pyspark.sql.types import *

sc = SparkContext(appName="CSV2Parquet")
spark = SQLContext(sc)

df = spark.read.load("/home/asad/Spark Practice/1500000 Sales Records.csv",
                      format='com.databricks.spark.csv',
                      header='true',
                      inferSchema='true')
df = df.withColumn("Order Date", to_date(unix_timestamp(df['Order Date'], 'MM/dd/yyy').cast("timestamp")))
df = df.withColumn("Ship Date", to_date(unix_timestamp(df['Ship Date'], 'MM/dd/yyy').cast("timestamp")))
for col in df.columns:
    df = df.withColumnRenamed(col,col.replace(" ", "_"))
#df.show()
#df.printSchema()
df.groupBy("Order_Priority").avg(("Unit_Price")).show(5)
df.write.mode('overwrite').partitionBy("Order_Priority","Sales_Channel").parquet("/home/asad/Spark Practice/Order.parquet")
parquetFile = spark.read.parquet("/home/asad/Spark Practice/Order.parquet/Order_Priority=L")
parquetFile.createOrReplaceTempView('order')
spark.sql("""select Order_Date from order""").show(5)
parquetFile.printSchema()
